# MasterProject2021

This Repository is dedicated to storing Master projects which requires analysis of Fake news 

#General guidelines to perform webscrapping 
1. check whether you can scrap the content 
2. You can use robot.txt suffix on the website.
Example: https://clashdaily.com/robots.txt

#Scripts used for Web scrapping:
webistescrap-americanlookout.com.ipynb
Webscrappingfakewebsite1.ipynb
webistescrap-realdataset.ipynb


#Main Files for Modleing and analysis
preprocessing.ipynb  - Mainly used to pre-process the data and perform analysis on the data
ModelingandLime.ipynb - Used for Modeling and analysis lime interperetaion
